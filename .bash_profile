# Setting PATH -------------------------------------------------------------------
export PATH="/Library/Frameworks/Python.framework/Versions/3.7/bin:${HOME}/bin:${HOME}/work/tools/working:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:"
export PATH="$HOME/.cargo/bin:$PATH"

# Set my prompt ------------------------------------------------------------------
host='\h'
working_dir='\w'
yellow="\[\033[0;93m\]"
white='\[\033[m\]'
colour1='\033[36m'
colour2='\033[32m'

export PS1="${yellow}[${host}] ${working_dir}:${white}\n"

# To get nice colours in my ls funciton ------------------------------------------
export LSCOLORS='fxafxxxxgxxxxxxxxxxxxx'

# Setting up aliases for personal commands ---------------------------------------
alias 'docker_del'='~/bin/docker/del_all_proc+images.sh'
alias lsa="ls -GAlhF"
alias ls="ls -GlhF"
alias ML='. ~/bin/ML_venv_switch'

# Variables -----------------------------------------------------------------------
homeserver='86.13.244.162'


# Nice welcome message -----------------------------------------------------------
ls
# Sourcing other files ------------------------------------------------------------
source ~/.bashrc


