#!/usr/bin/env bash

# Get absolute path
get_abs_path () {
	MY_PATH="`dirname \"$0\"`"              # relative
	MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized
	if [ -z "$MY_PATH" ] ; then
		  # error; for some reason, the path is not accessible
		  # to the script (e.g. permissions re-evaled after suid)
		  exit 1  # fail
	fi
	echo "$MY_PATH"
}

backup_dir () {
	dir_to_backup=$1

	git_dir=$(get_abs_path)
	git_alias="/usr/bin/git --git-dir=${git_dir} --work-tree=$HOME"

	if [ -e ${dir} ]
	then
		${git_alias} add ${dir_to_backup}
		${git_alias} commit -m "added ${dir_to_backup}"
	else
		echo "${dir} does not exist. It was not backed up."
	fi
}
