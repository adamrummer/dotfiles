#!/usr/bin/env bash


# =============================================================================================

source ../config.sh
source ../functions.sh

# ensure alias exists in relevant shellrc file
git_dir=$(get_abs_path)
set_alias='false'
for shellrc in ${shellrc_list}
do
	shellrc_loc="${HOME}/${shellrc}"
	if [ -e ${shellrc_loc} ]
	then
		git_alias="/usr/bin/git --git-dir=${git_dir} --work-tree=${HOME}"
		alias_line="alias ${alias_command}=${git_alias}"
		# If alias is already configured, then skip
		if grep -Fxq "${alias_line}" "${shellrc_loc}"
			then
				# code if found
				continue
			else
				# code if not found
				echo "writing alias:"
				echo "${alias_line}"
				echo "to ${shellrc_loc}"
				echo ${alias_line} >> "${shellrc_loc}"
				set_alias='true'
			fi

		# Hide files we are not explicitly tracking yet
		"${alias_command}" config --local status.showUntrackedFiles no
		if [ ${set_alias} == 'true']
		then
			alias ${alias_command}=${git_alias}
		fi
	fi
done

# Add files to .gitignore
for file in ${files_to_ignore}
do
	echo ${file} >> "${git_dir}/.gitignore"
done

echo "Making initial backup..."
for dotfile in ${dotfiles_to_backup}
do
	backup_dir(${dotfile})
	echo "--- Backed up ${dotfile}"
done
echo "Finished making initial backup."


# Add entry to crontab for this computer for backup schedule
check_crontab='false'
command="/usr/bin/env bash ${git_dir}/back_up.sh"
cron_line="${backup_time} ${command}"

# Checks if crontab exists at crontab_loc
if [ -e ${crontab_loc} ]
then
	check_crontab='true'
else
	echo "crontab not found at ${crontab_loc}, not writing ${cron_line}"
fi

if [ check_crontab == 'true' ]
then
		# If command is not in users crontab
		if [[ $(crontab -l | egrep -v "^(#|$)" | grep -q ${command}; echo $?) == 1 ]]
		then
			# Append cron_line to crontab
			echo "writing ${cron_line} to ${crontab_loc}"
  		echo "$(echo ${cron_line} ; crontab -l)" | crontab -
		else
			echo "$ already found in ${crontab_loc}, not writing."
		fi
fi
