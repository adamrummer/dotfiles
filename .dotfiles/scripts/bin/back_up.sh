#!/usr/bin/env bash

source ../config.sh
source ../functions.sh

# Back up files specified in ./config.sh
echo "commencing backup"
for dotfile in ${dotfiles_to_backup}
do
	backup_dir(dotfile)
	echo "--- Backed up ${dotfile}"
done
