#!/usr/bin/env bash

alias_name="dotfiles"
shellrc_list=( ".bashrc" ".zshrc" ".fishrc" )
backup_time="0 3 * * *"
crontab_loc="${HOME}/crontab"

dotfiles_to_backup=(
	'~/.vimrc'
	'~/.zshrc'
	'~/.oh-my-zsh'
	'~/.bashrc'
	'~/.bash_profile'
)
