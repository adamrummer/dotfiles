export PATH=$HOME/bin:/usr/bin:/bin:/usr/sbin:/sbin
export PATH='/Users/adamrummer/bin:/Library/Frameworks/Python.framework/Versions/3.7/bin:/usr/local/sbin:/Library/Frameworks/Python.framework/Versions/3.6/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin'

# Path to your oh-my-zsh installation.
export ZSH="${HOME}/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

HYPHEN_INSENSITIVE="true" # for auto completion, _ and - will be interchangeable
ENABLE_CORRECTION="true" # enable command auto-correction.

plugins=(
	zsh-syntax-highlighting
	zsh-autosuggestions
	colored-man-pages
)

source $ZSH/oh-my-zsh.sh


# PROMPT ----------------------------------------------------------------------


# Pretty colours
yellow=$FG[226]
purple=$FG[005]
reset=%{$reset_color%}

# Left prompt
export PS1="${yellow}[%n@%m${reset}${purple} %~${reset}${yellow}] :${reset}"

# Load git version control information
autoload -Uz vcs_info
precmd() { vcs_info }
# Format the vcs_info_msg_0_ variable
zstyle ':vcs_info:git:*' formats 'on branch %b'
# Right prompt
export RPROMPT=\$vcs_info_msg_0_ # prints current git branch


# To get nice colours in my ls funciton ------------------------------------------
export LSCOLORS='fxafxxxxgxxxxxxxxxxxxx'

# Setting up aliases for personal commands ---------------------------------------
alias 'docker_del'='~/bin/docker/del_all_proc+images.sh'
alias ls="ls -GlhF"
alias ML='. ~/bin/ML_venv_switch'

# Variables -----------------------------------------------------------------------
homeserver='86.13.244.162'

# Nice welcome message -----------------------------------------------------------
ls

# Autoloading functions --------------------------------------------------------
for file in ~/.oh-my-zsh/functions/*; do
	autoload -Uz $(basename "${file}")
done

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/adamrummer/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/adamrummer/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/adamrummer/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/adamrummer/google-cloud-sdk/completion.zsh.inc'; fi
